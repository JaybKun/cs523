#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <mpe.h>
//---------------------------------------------------------------------------
main (int argc, char** argv)
{
	MPI::Init(argc, argv);
	MPE_Init_log();	
	MPE_Start_log();

	MPE_Finish_log("log.clog");	
	MPI::Finalize();
	return 0;
}
//----------------------------------------------------------------------------

