#ifndef MPI_NODE_H
#define MPI_NODE_H
//------------------------------------------------------------
#include <mpi.h>
//------------------------------------------------------------
#define ROOT 0
#define MESSAGE_TAG 1000
#define WORK_TAG 2000
//------------------------------------------------------------
class mpiNode
{
public:
	mpiNode(int argc, char** argv);
	~mpiNode();

	int id();
	char* name();

	int send(char* pcMessage, int iDest);

	void getStatus(MPI_Status status, char* pcStatus); 
	void run();
	
protected:
	int m_iNodes;
	int m_ID;
	char* m_pcName;
	bool m_bRoot;
};
//------------------------------------------------------------
#endif

