#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <mpi.h>
//---------------------------------------------------------------------------
#define ROOT 0

#define MESSAGE_TAG 1000
#define WORK_TAG    1001
//---------------------------------------------------------------------------
#define SWARM_WEIGHT 2.5f
#define SELF_WEIGHT 1.5f
#define GENERATIONS 32 

#define XMIN 0.0f
#define XMAX 4.0f

#define YMIN -2.0f
#define YMAX 3.0f

#define VEL_MAX 0.1f
#define VEL_MIN -0.1f
//---------------------------------------------------------------------------
struct Position {
    float x;
    float y;
};

typedef struct Particle {
    struct Position current_pos;
    struct Position velocity;

    float current_fitness;
    
    struct Position best_pos;
    float best_fitness;
} MPI_PARTICLE;
//---------------------------------------------------------------------------
// Algorithm Functions
float fitness(struct Particle& f, bool bSetBest = false);
float randFloat(float max, float min);
void  printParticle(struct Particle p);
//---------------------------------------------------------------------------
// MPI Functions
void getStatus(MPI_Status status, char* pcStatus);
//---------------------------------------------------------------------------
main (int argc, char** argv)
{
  int iNameLen;
  int iSwarmSize;

  Particle particle;
  int id;
  
  char* pcName    = (char*)malloc(1024);
  char* pcMessage = (char*)malloc(1024);
  char* pcStatus  = (char*)malloc(1024);
  
  MPI_Init(&argc, &argv);
  MPI_Status status;
 
  srand(time(NULL));

  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  MPI_Comm_size(MPI_COMM_WORLD, &iSwarmSize);
  MPI_Get_processor_name(pcName, &iNameLen);
  
  bool bRoot = (id == ROOT);
  particle.current_pos.x = randFloat(0, 4.0); 
  particle.velocity.x   = randFloat(0.0, 0.5); 
  particle.current_pos.y = randFloat(0, 4.0); 
  particle.velocity.y   = randFloat(0.0, 0.5); 
  
  if (particle.current_pos.x > XMAX) particle.current_pos.x = XMAX;
  if (particle.current_pos.x < XMIN) particle.current_pos.x = XMIN;
  if (particle.current_pos.y > YMAX) particle.current_pos.y= YMAX;
  if (particle.current_pos.y < YMIN) particle.current_pos.y= YMIN;
 
  fitness(particle, true);
  
  if (!bRoot) {
    sprintf(pcMessage, "%i, %s", id, pcName);
    MPI_Send(pcMessage, strlen(pcMessage)+1, MPI_CHAR, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
  } else {
    printf("Initializing Nodes\n");
    printf("*********************************************************\n");
    printf("Root Node: %s\n", pcName);
    printf("Awaiting worker node responses...\n");
    for (int iSource = 1; iSource < iSwarmSize; iSource++) {
      MPI_Recv(pcMessage, 1024, MPI_CHAR, iSource, MESSAGE_TAG, MPI_COMM_WORLD, &status);
      getStatus(status, pcStatus);
      printf("AgentNode: %s\t| Status: %s\n", pcMessage, pcStatus);
    }
    printf("Node initialization complete...\n");
    printf("%i Nodes Registered for work...\n", iSwarmSize);
    printf("*********************************************************\n");
  }

  int iSwarmBest = 0;
  Particle pSwarmBest;
  
  if (bRoot) {
    pSwarmBest.best_fitness = particle.current_fitness;
    pSwarmBest.best_pos.x   = particle.current_pos.x;
    pSwarmBest.best_pos.y   = particle.current_pos.y;
    
    Particle tmpParticle;
    for (int iParticle = 1; iParticle < iSwarmSize; iParticle++) {
      MPI_Recv(&tmpParticle.current_fitness, 1, MPI_FLOAT, iParticle, MESSAGE_TAG, MPI_COMM_WORLD, &status);
      MPI_Recv(&tmpParticle.current_pos.x,   1, MPI_FLOAT, iParticle, MESSAGE_TAG, MPI_COMM_WORLD, &status);
      MPI_Recv(&tmpParticle.current_pos.y,   1, MPI_FLOAT, iParticle, MESSAGE_TAG, MPI_COMM_WORLD, &status);
      if (tmpParticle.current_fitness > pSwarmBest.best_fitness) {
        iSwarmBest              = iParticle;
        pSwarmBest.best_fitness = tmpParticle.current_fitness;
        pSwarmBest.best_pos.x   = tmpParticle.current_pos.x;
        pSwarmBest.best_pos.y   = tmpParticle.current_pos.y;
      }
    }
    printParticle(pSwarmBest);
  } else {
    MPI_Send(&particle.current_fitness, 1, MPI_FLOAT, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
    MPI_Send(&particle.current_pos.x,   1, MPI_FLOAT, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
    MPI_Send(&particle.current_pos.y,   1, MPI_FLOAT, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
  }  

  MPI_Bcast(&pSwarmBest.best_fitness, 1, MPI_FLOAT, ROOT, MPI_COMM_WORLD);
  MPI_Bcast(&pSwarmBest.best_pos.x,   1, MPI_FLOAT, ROOT, MPI_COMM_WORLD);
  MPI_Bcast(&pSwarmBest.best_pos.y,   1, MPI_FLOAT, ROOT, MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD);

  for (int iGeneration = 0; iGeneration < GENERATIONS; iGeneration++) {
    if (bRoot) {
      printf("Generation-> %i\n", iGeneration);
    }
   
    float fSwarmRand = randFloat(0, 1); 
    float fSelfRand  = randFloat(0, 1);
    float fNewVel;
    fNewVel = particle.velocity.x + 
              SELF_WEIGHT  * (particle.best_pos.x - particle.current_pos.x)  * fSelfRand + 
              SWARM_WEIGHT * (pSwarmBest.best_pos.x - particle.current_pos.x) * fSwarmRand;

    if (fNewVel > VEL_MAX) fNewVel = VEL_MAX;
    if (fNewVel < VEL_MIN) fNewVel = VEL_MIN;
    particle.current_pos.x += fNewVel;
    particle.velocity.x = fNewVel;

    if (particle.current_pos.x > XMAX) particle.current_pos.x = XMAX;
    if (particle.current_pos.x < XMIN) particle.current_pos.x = XMIN;
    if (particle.current_pos.y > YMAX) particle.current_pos.y = YMAX;
    if (particle.current_pos.y < YMIN) particle.current_pos.y = YMIN;

    fitness(particle);
    printParticle(particle);

    if (bRoot) {
      Particle tmpParticle;
      for (int iParticle = 1; iParticle < iSwarmSize; iParticle++) {
        MPI_Recv(&tmpParticle.current_fitness, 1, MPI_FLOAT, iParticle, MESSAGE_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&tmpParticle.current_pos.x,   1, MPI_FLOAT, iParticle, MESSAGE_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&tmpParticle.current_pos.y,   1, MPI_FLOAT, iParticle, MESSAGE_TAG, MPI_COMM_WORLD, &status);
      
        if (tmpParticle.current_fitness > pSwarmBest.best_fitness) {
          iSwarmBest              = iParticle;
          pSwarmBest.best_fitness = tmpParticle.current_fitness; 
          pSwarmBest.best_pos.x   = tmpParticle.current_pos.x;
          pSwarmBest.best_pos.y   = tmpParticle.current_pos.y;
        }
      }
    } else {
      MPI_Send(&particle.current_fitness, 1, MPI_FLOAT, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
      MPI_Send(&particle.current_pos.x,   1, MPI_FLOAT, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
      MPI_Send(&particle.current_pos.y,   1, MPI_FLOAT, ROOT, MESSAGE_TAG, MPI_COMM_WORLD);
    } 
    
    MPI_Bcast(&pSwarmBest.best_fitness, 1, MPI_FLOAT, ROOT, MPI_COMM_WORLD);
    MPI_Bcast(&pSwarmBest.best_pos.x,   1, MPI_FLOAT, ROOT, MPI_COMM_WORLD);
    MPI_Bcast(&pSwarmBest.best_pos.y,   1, MPI_FLOAT, ROOT, MPI_COMM_WORLD);
    if (bRoot) {
      printf("SwarmBest: ");
      printParticle(pSwarmBest);
    }
  }

  if (bRoot) {
    printf("And the winner is: (%f, %f) -> %f\n",  pSwarmBest.best_pos.x, pSwarmBest.best_pos.y, pSwarmBest.best_fitness);
  }
 
  MPI_Finalize();
  
  return 0; 
}
//-------------------------------------------------
float randFloat(float a, float b)
{
  float random = ((float) rand()) / (float) RAND_MAX;
  float diff = b - a;
  float r = random * diff;
  return a + r; 
}
//-------------------------------------------------
float fitness(struct Particle& f, bool bSetBest)
{
    float x = f.current_pos.x;
    float y = f.current_pos.y;

    f.current_fitness =  -1 * pow(x - 0.5, 3) + x;
    //f.current_fitness =  -1 * pow(x - 0.5, 3) + x - pow(y, 2) - (x * y);

    if (f.current_fitness > f.best_fitness || bSetBest == true) {
      f.best_fitness = f.current_fitness;
      f.best_pos.x   = x;
      f.best_pos.y   = y;
    }
    return f.current_fitness;
}
//-------------------------------------------------
void printParticle(struct Particle p)
{
  printf("(%f @ %f) -> %f  best: (%f) -> %f\n", p.current_pos.x, p.velocity.x, p.current_fitness, p.best_pos.x, p.best_fitness);
}
//-------------------------------------------------
void getStatus(MPI_Status status, char* pcStatus)
{
switch (status.MPI_ERROR) {
	case MPI_SUCCESS:
		sprintf(pcStatus, "Success");
		break;
	case MPI_ERR_BUFFER:
		sprintf(pcStatus, "Buffer Error");
		break;
	case MPI_ERR_COUNT:
		sprintf(pcStatus, "Count Error");
		break;
	case MPI_ERR_TYPE:
		sprintf(pcStatus, "Type Error");
		break;
	case MPI_ERR_TAG:
		sprintf(pcStatus, "Tag Error");
		break;
	case MPI_ERR_COMM:
		sprintf(pcStatus, "COMM Error");
		break;
	case MPI_ERR_RANK:
		sprintf(pcStatus, "Rank Error");
		break;
	case MPI_ERR_REQUEST:
		sprintf(pcStatus, "Request Error");
		break;
	case MPI_ERR_ROOT:
		sprintf(pcStatus, "Root Error");
		break;
	case MPI_ERR_GROUP:
		sprintf(pcStatus, "Group Error");
		break;	
	case MPI_ERR_OP:
		sprintf(pcStatus, "OP Error");
		break;
	case MPI_ERR_TOPOLOGY:
		sprintf(pcStatus, "Topology Error");
		break;
	case MPI_ERR_DIMS:
		sprintf(pcStatus, "DIMS Error");
		break;
	case MPI_ERR_ARG:
		sprintf(pcStatus, "Args Error");
		break;
	case MPI_ERR_UNKNOWN:
		sprintf(pcStatus, "Unknown Error");
		break;
	case MPI_ERR_TRUNCATE:
		sprintf(pcStatus, "Truncate Error");
		break;	
	case MPI_ERR_OTHER:
		sprintf(pcStatus, "Other Error");
		break;	
	case MPI_ERR_INTERN:
		sprintf(pcStatus, "Internal Error");
		break;
	case MPI_ERR_IN_STATUS:
		sprintf(pcStatus, "In Status Error");
		break;
	case MPI_ERR_PENDING:
		sprintf(pcStatus, "Pending Error");
		break;
	default:
		sprintf(pcStatus, "Unknown Error");
  }
}
//------------------------------------------------

