#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* runnable(void* param);

#define ROWS 2
#define COLS 2

int A[ROWS][COLS] = { { 0, 5 }, { 3, 1 } };
int B[ROWS][COLS] = { { 4, 1 }, {-5, -2 } };
int RESULTS[ROWS][COLS];

struct cell {
  int row;
  int col;
};

int main(int argc, char** argv)
{
  int iTCount = 0;
  for (int i = 0; i < ROWS; i++) {
    int* row = (int*)malloc(COLS * sizeof(int)); 
    for (int col = 0; col < COLS; col++) {
      struct cell* data = (struct cell*)malloc(sizeof(struct cell));
      data->row = i;
      data->col = col;

      pthread_t tid;
      pthread_create(&tid, NULL, runnable, data);
      pthread_join(tid, NULL);
      
      iTCount++;
    }
  }

  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      printf("%d ", RESULTS[i][j]);
    }
    printf("\n");
  }
}

void* runnable(void* param)
{
  struct cell* data = (struct cell*)param;
  int sum = 0;

  for (int n = 0; n < ROWS; n++) {
    sum += A[data->row][data->col] * B[data->row][data->col];
  }  
  RESULTS[data->row][data->col] = sum;
  pthread_exit(0);
}

