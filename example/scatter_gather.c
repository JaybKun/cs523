const int recvsize = 50;
int *sendbuf, recvbuf[recvsize];
int sendsize = nb_proc*recvsize;
sendbuf = new int[sendsize];
if (proc_id == 0)
  Generate_data(sendbuf, sendsize);
MPI_Scatter(sendbuf, recvsize, MPI_INT, recvbuf, recvsize, MPI_INT, 0, MPI_COMM_WORLD);
for (i=0; i<nb_proc; i++)
  Print_data(recvbuf, recvsize);
Example Using Gather

const int sendsize = 50;
int sendbuf[sendsize], *recvbuf;
int recvsize = nb_proc*sendsize; 
if (proc_id == 0)
  recvbuf = new int[recvsize];
for (i=0; i<nb_proc; i++)
  Generate_data(sendbuf, sendsize);
MPI_Gather(sendbuf, sendsize, MPI_INT, recvbuf, sendsize, MPI_INT, 0, MPI_COMM_WORLD);
if (proc_id == 0)
  Print_data(recvbuf, recvsize);
