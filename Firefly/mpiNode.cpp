#include "mpiNode.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//-------------------------------------------------
mpiNode::mpiNode(int argc, char** argv)
{
	int iNameLen;
	char* m_pcName = (char*)malloc(1024);
	char* pcMessage = (char*)malloc(1024);
	char* pcStatus = (char*)malloc(1024);
	int iSource;
	
	MPI_Status status;		
	
	m_ID    = MPI::COMM_WORLD.Get_rank();
	m_bRoot = (m_ID == 0);
	m_iNodes = MPI::COMM_WORLD.Get_size() - 1; // subtract 1 to not count the root node
	MPI::Get_processor_name(m_pcName, iNameLen);

	if (!m_bRoot) {
		sprintf(pcMessage, "%i", m_ID, m_pcName);
		send(pcMessage, ROOT);
	} else {
		printf("Initializing Nodes\n");
		printf("*********************************************************\n");
		printf("Root Node: %s\n", m_pcName);
		//printf("Awaiting worker node responses...\n");
		/*for (iSource = 1; iSource <= m_iNodes; iSource++) {
			MPI_Recv(pcMessage, 1024, MPI_CHAR, iSource, MESSAGE_TAG, MPI_COMM_WORLD, &status);
			getStatus(status, pcStatus);
			printf("AgentNode: %s\t| Status: %s\n", pcMessage, pcStatus);
		}*/
		printf("Node initialization complete...\n");
		printf("%i Nodes Registered for work...\n", m_iNodes);
		printf("*********************************************************\n");
	}
	free(pcMessage);
	free(pcStatus);
}
//-------------------------------------------------
mpiNode::~mpiNode()
{
  printf("Node %i destroyed\n", m_pcName);
  //free(m_pcName); 
}
//-------------------------------------------------
int mpiNode::id()
{
  return m_ID;
}
//-------------------------------------------------
char* mpiNode::name()
{
  return m_pcName;
}
//-------------------------------------------------
int mpiNode::send(char* pcMessage, int iDest)
{
  int rc;
  rc = MPI_Send(pcMessage, strlen(pcMessage)+1, MPI_CHAR, iDest, MESSAGE_TAG, MPI_COMM_WORLD);
  return rc;
}
//------------------------------------------------
void mpiNode::getStatus(MPI_Status status, char* pcStatus)
{
	switch (status.MPI_ERROR) {
		case MPI_SUCCESS:
			sprintf(pcStatus, "Success");
			break;
		case MPI_ERR_BUFFER:
			sprintf(pcStatus, "Buffer Error");
			break;
		case MPI_ERR_COUNT:
			sprintf(pcStatus, "Count Error");
			break;
		case MPI_ERR_TYPE:
			sprintf(pcStatus, "Type Error");
			break;
		case MPI_ERR_TAG:
			sprintf(pcStatus, "Tag Error");
			break;
		case MPI_ERR_COMM:
			sprintf(pcStatus, "COMM Error");
			break;
		case MPI_ERR_RANK:
			sprintf(pcStatus, "Rank Error");
			break;
		case MPI_ERR_REQUEST:
			sprintf(pcStatus, "Request Error");
			break;
		case MPI_ERR_ROOT:
			sprintf(pcStatus, "Root Error");
			break;
		case MPI_ERR_GROUP:
			sprintf(pcStatus, "Group Error");
			break;	
		case MPI_ERR_OP:
			sprintf(pcStatus, "OP Error");
			break;
		case MPI_ERR_TOPOLOGY:
			sprintf(pcStatus, "Topology Error");
			break;
		case MPI_ERR_DIMS:
			sprintf(pcStatus, "DIMS Error");
			break;
		case MPI_ERR_ARG:
			sprintf(pcStatus, "Args Error");
			break;
		case MPI_ERR_UNKNOWN:
			sprintf(pcStatus, "Unknown Error");
			break;
		case MPI_ERR_TRUNCATE:
			sprintf(pcStatus, "Truncate Error");
			break;	
		case MPI_ERR_OTHER:
			sprintf(pcStatus, "Other Error");
			break;	
		case MPI_ERR_INTERN:
			sprintf(pcStatus, "Internal Error");
			break;
		case MPI_ERR_IN_STATUS:
			sprintf(pcStatus, "In Status Error");
			break;
		case MPI_ERR_PENDING:
			sprintf(pcStatus, "Pending Error");
			break;
		default:
			sprintf(pcStatus, "Unknown Error");
	}
}
//------------------------------------------------
void mpiNode::run()
{
}
//------------------------------------------------

