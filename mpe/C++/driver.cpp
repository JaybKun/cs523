//--- Test driver for class Time

#include <stdlib.h>
#include <iostream>
using namespace std;
#include "Time.h"

#include "mpi.h"
#include "mpe.h"

extern "C" {
//int MPE_Log_get_event_number(void);
int MPE_Log_get_state_eventIDs(int *, int *);
}
#ifdef SES
extern "C" {
int CLOG_Get_user_eventID (void);
}
#endif


//int main()
int main(int argc, char *argv[])

{
   Time mealTime;
   int event1a, event1b, event2a, event2b,
        event3a, event3b, event4a, event4b;
   int  myid, numprocs;
   int myStatus;


   MPI_Init( &argc, &argv );
   MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
   MPI_Comm_rank(MPI_COMM_WORLD,&myid);


   printf("after MPE_Init_log myrank is: %d\n", myid);
   fflush(stdout);

   MPI_Barrier( MPI_COMM_WORLD );

   /*  Get event ID from MPE, user should NOT assign event ID  */
//int MPE_Log_get_state_eventIDs( int *statedef_startID,
                                //int *statedef_finalID )
   myStatus = MPE_Log_get_state_eventIDs(&event1a, &event1b);

   //event1a = MPE_Log_get_event_number();
   printf("after events\n");
   fflush(stdout);
   //event1b = MPE_Log_get_event_number();

   //exit(0);
   event2a = MPE_Log_get_event_number();
   event2b = MPE_Log_get_event_number();

   printf("after event2 event2a=%d event2b=%d\n",event2a,event2b);

   event3a = MPE_Log_get_event_number();
   event3b = MPE_Log_get_event_number();
   event4a = MPE_Log_get_event_number();
   event4b = MPE_Log_get_event_number();

   printf("after events 2\n");
   fflush(stdout);

   if (myid == 0) {
       //MPE_Describe_state(event1a, event1b, "Broadcast", "red");
       //MPE_Describe_state(event2a, event2b, "Compute",   "blue");
       //MPE_Describe_state(event3a, event3b, "Reduce",    "green");
       //MPE_Describe_state(event4a, event4b, "Sync",      "orange");
       MPE_Describe_state(event1a, event1b, "Scott", "red");
       MPE_Describe_state(event2a, event2b, "Exelis",   "blue");
       MPE_Describe_state(event3a, event3b, "TimeCpp",    "green");
       MPE_Describe_state(event4a, event4b, "CS340",      "orange");
   }

   printf("after MPE_Describe_state\n");
   fflush(stdout);


   mealTime.set(5, 30, 'P');

   cout << "We'll be eating at ";
   mealTime.display(cout);
   cout << endl;

   MPE_Start_log();

   MPE_Log_event(event1a, 0, "start PRINTF");
   printf("after MPE_Start_log, numprocs=%d\n",numprocs);
   fflush(stdout);
   MPE_Log_event(event1b, 0, "end PRINTF");

   MPE_Log_event(event2a, 0, "start compute");
   cout << "\nNow trying to set time with illegal hours (13)" << endl;
   mealTime.set(13, 0, 'A');
   MPE_Log_event(event2b, 0, "end compute");

   MPE_Log_event(event3a, 0, "start mealTime");
        //MPI_Reduce(&mypi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
   cout << "Now trying to set time with illegal minutes (60)" << endl;
   mealTime.set(5, 60, 'A');
   MPE_Log_event(event3b, 0, "end mealTime");

   cout << "Now trying to set time with illegal AM/PM ('X')" << endl;
   mealTime.set(5, 30, 'X'); 

   MPI_Finalize();
}
